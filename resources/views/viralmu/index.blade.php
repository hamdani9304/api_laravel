@extends('welcome')
@section('master')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Api</a>
                            </li>
                            <li class="breadcrumb-item active">Data Api Viralmu
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Zero configuration table -->
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Data Api Viralmu</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr style="text-align: center">
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Ipaymu Email</th>
                                                <th>Referral Key</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                        
                                            @foreach ($h as $data)
                                            <tr>
                                                <td style="text-align: center">{{ $data['id'] }}</td>
                                                <td style="text-align: center">{{ $data["name"] }}</td>
                                                <td>{{ $data['email'] }}</td>
                                                <td style="text-align: center">{{ $data['phone'] }}</td>
                                                <td style="text-align: center">{{ $data['ipaymu_email'] }}</td>
                                                <td style="text-align: center">{{ $data['referral_key'] }}</td>
                                               
                                            @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
{{-- 12345543451 --}}