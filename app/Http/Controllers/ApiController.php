<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
// use GuzzleHttp\Client\Request;

class ApiController extends Controller
{
    
    public function index()
    {
        $http = new \GuzzleHttp\Client;
        $data = $http->get('https://app.viralmu.id/api/open/get-profile', [
            'headers'=>[
                'viralkey' => 'viral375154',
                'signature' => 'OGM2ZjRjZDAtYzYxMi0xMWViLWE5MjgtMzNjNjk2MDM1MTk1OnZpcmFsMzc1MTU0'
            ],
        ]);

        $result = json_decode((string)$data->getBody(),true);
        $d = array_filter($result);
        $h = collect($d)->where('id');

        return view('viralmu.index', compact('h'));
        
        // $client = new http\Client;
        // $request = new \GuzzleHttp\Client\Request;
        // $request->setRequestUrl('https://app.viralmu.id/api/open/get-profile');
        // $request->setRequestMethod('GET');
        // $request->setOptions(array());
        // $request->setHeaders(array(
        //     'viralkey' => 'viral698',
        //     'signature' => 'MEIwOTIzRTEtNkNFNS00REJFLTlEOTAtRkIxNDUzOUZERkUyOnZpcmFsNjk4'
        // ));
        // $http->enqueue($request)->send();
        // $response = $http->getResponse();
        // echo $response->getBody();

        // hellow   
        // test aja
        // 1234565454466464

    }
}
